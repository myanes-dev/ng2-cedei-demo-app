import { Ng2CedeiDemoAppPage } from './app.po';

describe('ng2-cedei-demo-app App', function() {
  let page: Ng2CedeiDemoAppPage;

  beforeEach(() => {
    page = new Ng2CedeiDemoAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
