import { Person, PeopleService } from './../../services/people.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contacts-list',
  templateUrl: './contacts-list.component.html',
  styleUrls: ['./contacts-list.component.css']
})
export class ContactsListComponent implements OnInit {

  peopleList: Person[];

  // Dependency Injection
  constructor(private peopleService: PeopleService) { }

  ngOnInit() {
    this.peopleList = this.peopleService.getPeople();
  }

  deletePerson(index: number) {
    this.peopleList.splice(index, 1);
  }

}
