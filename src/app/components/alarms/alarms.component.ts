import { AlarmsService, Alarm } from './../../services/alarms.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-alarms',
  templateUrl: './alarms.component.html',
  styleUrls: ['./alarms.component.css']
})
export class AlarmsComponent implements OnInit {

  newTime: number;
  newTitle: string;
  showTimePicker: boolean;

  alarmsList: Alarm[];

  constructor(private alarmsService: AlarmsService) { }

  ngOnInit() {
    this.alarmsList = this.alarmsService.getAlarms();
  }

  addAlarm() {
    let newAlarm = new Alarm();
    newAlarm.time = this.newTime;
    newAlarm.title = this.newTitle;

    if (this.validValues()) {
      this.alarmsList.push(newAlarm);
      this.resetValues();
    }

  }

  onTimeChanged(newTime: number) {
    this.newTime = newTime;
    this.showTimePicker = false;
  }

  resetValues() {
    this.newTime = null;
    this.newTitle = null;
  }

  validValues(): boolean {
    return this.newTitle != null && this.newTitle.trim() !== '' && this.newTime != null;

  }



}
