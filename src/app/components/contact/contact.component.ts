import { Person } from './../../services/people.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  @Input() contact: Person;
  @Output() onDelete = new EventEmitter();

  // bool to set remove animation -> see template './contact.component.html'
  byebye: boolean;

  constructor() { }

  ngOnInit() { }

  onClick() {
    this.byebye = true;
    setTimeout(() => {
      this.onDelete.emit();
    }, 1000);

  }

}
