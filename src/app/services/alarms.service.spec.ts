/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { AlarmsService } from './alarms.service';

describe('AlarmsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AlarmsService]
    });
  });

  it('should ...', inject([AlarmsService], (service: AlarmsService) => {
    expect(service).toBeTruthy();
  }));
});
