import { alarmsData } from './../data/alarms';
import { Injectable } from '@angular/core';

@Injectable()
export class AlarmsService {

  constructor() { }

  // Fetch people data --mock
  // TODO get real data from any database -> see: https://angular.io/docs/ts/latest/tutorial/toh-pt6.html
  getAlarms(): Alarm[] {
    let alarmsList: Alarm[] = alarmsData.concat();
    return alarmsList;
  }
}

// Object to bind the alarmsData. It is good practice to be placed in a different file.
export class Alarm {
  title: string;
  time: number;
}


