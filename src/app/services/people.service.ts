import { peopleData } from './../data/people';
import { Injectable } from '@angular/core';

@Injectable()
export class PeopleService {

  constructor() { }

  // Fetch people data --mock
  // TODO get real data from any database -> see: https://angular.io/docs/ts/latest/tutorial/toh-pt6.html
  getPeople(): Person[] {
    let peopleList: Person[] = peopleData.concat();
    return peopleList;
  }
}

// Object to bind the peopleData. It is good practice to be placed in a different file.
export class Person {
  name: string;
  about: string;
  phone: string;
}
