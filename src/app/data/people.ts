export let peopleData =
  [
    {
      'name': 'Bean Kidd',
      'about': 'Irure minim aute amet eu nisi duis aliquip deserunt pariatur eu labore id cillum minim. Mollit deserunt in \
      quis adipisicing duis dolor adipisicing cillum eu cupidatat. Amet laborum occaecat pariatur proident incididunt ea tempor.',
      'phone': '+1 (861) 513-3828'
    },
    {
      'name': 'Adrian Huber',
      'about': 'Eiusmod ex adipisicing veniam sunt anim et labore deserunt sunt. Enim sunt sint irure do enim amet est minim occaecat \
      voluptate in anim fugiat voluptate. Reprehenderit excepteur et culpa exercitation quis ex reprehenderit officia.',
      'phone': '+1 (997) 485-3037'
    },
    {
      'name': 'Annie Mason',
      'about': 'Sit ex nisi ex irure et mollit eiusmod enim. Exercitation ea non officia aliqua sunt consequat consectetur.',
      'phone': '+1 (905) 432-3041'
    },
    {
      'name': 'Roth Crosby',
      'about': 'Consectetur et sint irure id non. Minim velit non aute qui laboris ullamco Lorem. Deserunt occaecat Lorem et nulla.',
      'phone': '+1 (829) 593-2793'
    },
    {
      'name': 'Augusta Wheeler',
      'about': 'Minim excepteur laboris duis deserunt qui sit. Quis elit anim et aute do. Quis aute cupidatat pariatur excepteur.',
      'phone': '+1 (939) 585-3434'
    },
    {
      'name': 'Trujillo Forbes',
      'about': 'Aute aliqua proident anim nostrud laborum pariatur velit aliquip laboris ad. Esse minim nostrud\
       esse eu in nulla dolor laboris dolor cupidatat occaecat sunt elit adipisicing. Id est excepteur veniam cillum ex sint.',
      'phone': '+1 (814) 489-3522'
    }
  ];
