import { TimePickerModule } from './modules/time-picker/time-picker.module';
import { AlarmsService } from './services/alarms.service';
import { PeopleService } from './services/people.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { FooterComponent } from './components/footer/footer.component';
import { ContactsListComponent } from './components/contacts-list/contacts-list.component';
import { ContactComponent } from './components/contact/contact.component';
import { AlarmsComponent } from './components/alarms/alarms.component';
import { ExamplesComponent } from './components/examples/examples.component';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    FooterComponent,
    ContactsListComponent,
    ContactComponent,
    AlarmsComponent,
    ExamplesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(
      [
        { path: 'contacts', component: ContactsListComponent },
        { path: 'alarms', component: AlarmsComponent },
        { path: 'examples', component: ExamplesComponent },
        { path: '**', redirectTo: '/contacts', pathMatch: 'full' }
      ]
    ),
    TimePickerModule
  ],
  providers: [PeopleService, AlarmsService],
  bootstrap: [AppComponent]
})
export class AppModule {
 }
