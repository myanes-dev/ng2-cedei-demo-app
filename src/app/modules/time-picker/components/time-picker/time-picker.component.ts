import { DateOutils } from '../../utils/date-outils';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-time-picker',
  templateUrl: './time-picker.component.html',
  styleUrls: ['./time-picker.component.css']
})
export class TimePickerComponent implements OnInit {

  @Output() timeChanged = new EventEmitter<number>();
  @Output() cancel = new EventEmitter<any>();

  showHours = true;
  hours = 12;
  minutes: string = '00';
  am: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  updateTime() {
    let hours = this.am ? this.hours : this.hours + 12;
    hours = hours === 24 ? 12 : hours === 12 ? 0 : hours;
    let time = DateOutils.hours2Mils(hours) + DateOutils.mins2Mils(Number(this.minutes));
    this.timeChanged.emit(time);
  }

  onCancel() {
    this.cancel.emit();
  }

  changeHours(hour: number) {
    this.hours = hour;
    this.showHours = false;
  }

}
