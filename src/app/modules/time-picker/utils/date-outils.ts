export class DateOutils {

    public static hours2Mils(hours: number): number {
        return hours * 60 * 60 * 1000;
    }

    public static mins2Mils(minutes: number): number {
        return minutes * 60 * 1000;
    }

}