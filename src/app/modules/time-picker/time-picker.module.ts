import { TimePickerComponent } from './components/time-picker/time-picker.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [TimePickerComponent],
  exports: [TimePickerComponent]
})
export class TimePickerModule { }
